
package ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour doWork complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="doWork">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="n" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="corrId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "doWork", propOrder = {
    "n",
    "corrId"
})
public class DoWork {

    protected int n;
    protected int corrId;

    /**
     * Obtient la valeur de la propriété n.
     * 
     */
    public int getN() {
        return n;
    }

    /**
     * Définit la valeur de la propriété n.
     * 
     */
    public void setN(int value) {
        this.n = value;
    }

    /**
     * Obtient la valeur de la propriété corrId.
     * 
     */
    public int getCorrId() {
        return corrId;
    }

    /**
     * Définit la valeur de la propriété corrId.
     * 
     */
    public void setCorrId(int value) {
        this.corrId = value;
    }

}
