/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.QueueingConsumer;
import java.io.IOException;
import java.util.Scanner;
import java.util.UUID;
import ws.IOException_Exception;
import ws.TimeoutException_Exception;

/**
 *
 * @author Anthony
 */
public class Client 
{

    public static void main(String[] argv) throws Exception {
        //TODO: declaration des variables en haut
        Scanner sc = new Scanner(System.in);
        int id;

        int load;

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("ec2-54-157-2-56.compute-1.amazonaws.com");
        final Connection connection = factory.newConnection();
        final Channel channel = connection.createChannel();

        //Message persistant 'true'
        channel.queueDeclare("CALLBACK_QUEUE", true, false, false, null);

        System.out.println(" [*] Waiting for worker's acknowledgement");

        channel.basicQos(1);

        channel.basicQos(1);
  
      QueueingConsumer consumer = new QueueingConsumer(channel);
        
            System.out.println("Veuillez choisir une charge de travail en ms");
            System.out.println("-1 pour quitter");
            load = sc.nextInt();
            if (load != -1) {
                try {
                    //modification pour que result soit le temps passer dans la queue
                    id = 1;
                    //String uniqueID = UUID.randomUUID().toString();
                    doWork(load, id);
                    

                } catch (Exception ex) 
                {
                    System.out.println("Exception: " + ex);
                }
                finally
                {
                   //TODO depiler la callBackQueue jusqu'au bon uniqueID
                    channel.basicConsume("CALLBACK_QUEUE", false, consumer);
                }

            }
          
        System.out.println("Merci d'avoir fait travailler le worker ;)");
                       
        channel.close();
        connection.close();
    }

    private static void doWork(int n, int corrId) throws IOException_Exception, TimeoutException_Exception {
        ws.ProjectWS service = new ws.ProjectWS();
        ws.LoadManager port = service.getLoadManagerPort();
        port.doWork(n, corrId);
    }

}
