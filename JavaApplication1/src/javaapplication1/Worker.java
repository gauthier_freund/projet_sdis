/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1;

/**
 *
 * @author Anthony
 */
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.MessageProperties;
import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class Worker {

    private static final String TASK_QUEUE_NAME = "Working_Queue_Fio2";

    public static void main(String[] argv) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("ec2-54-157-2-56.compute-1.amazonaws.com");
        final Connection connection = factory.newConnection();
        final Channel channel = connection.createChannel();

        //Message persistant 'true'
        channel.queueDeclare(TASK_QUEUE_NAME, true, false, false, null);

        System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

        //1 message par worker à la fois 
        int prefetchCount = 1;
        channel.basicQos(prefetchCount);

        //instance queue de callback
        Channel channelAck = connection.createChannel();
        channelAck.queueDeclare("CALLBACK_QUEUE", true, false, false, null);

        final Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {

                String message = new String(body, "UTF-8");
                int sleepTime = Integer.parseInt(message);
                System.out.println(" [x] Received '" + sleepTime + "'");
                try {
                    doWork(sleepTime);

                } finally {
                    channel.basicAck(envelope.getDeliveryTag(), false);
                    //envoyer un message dans la queue de retour 
                    String messageAck = "Message ok";
                    channelAck.basicPublish("", "CALLBACK_QUEUE",
                            MessageProperties.PERSISTENT_TEXT_PLAIN,
                            messageAck.getBytes());
                    System.out.println("Message sent");
                }
            }
        };

        channel.basicConsume(TASK_QUEUE_NAME, false, consumer);

    }

    private static void doWork(int sleepTime) 
    {

       try 
        {
          Thread.sleep(sleepTime);
        } catch (InterruptedException _ignored) 
        {
          Thread.currentThread().interrupt();
        }
    }
}
