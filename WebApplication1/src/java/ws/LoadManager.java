/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ws;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.MessageProperties;
import java.io.IOException;
import java.util.concurrent.TimeoutException;
import javax.jws.Oneway;
import javax.xml.ws.RequestWrapper;

/**
 *
 * @author Anthony
 */
@WebService(serviceName = "projectWS")
public class LoadManager {

    private static final String TASK_QUEUE_NAME = "Working_Queue_Fio2";

    /**
     * Web service operation
     */
    @WebMethod(operationName = "doWork")
    public void doWork(@WebParam(name = "n") int n, @WebParam(name = "corrId") int corrId) throws IOException, TimeoutException 
    {
         ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("ec2-54-157-2-56.compute-1.amazonaws.com");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        //Message persistant 'true'
        channel.queueDeclare(TASK_QUEUE_NAME, true, false, false, null);

        String message = Integer.toString(n);
        //String message = "ID : " + corrId + " load : " + Integer.toString(n);
        channel.basicPublish("", TASK_QUEUE_NAME,
                MessageProperties.PERSISTENT_TEXT_PLAIN,
                message.getBytes());

        System.out.println(" [x] Sent '" + message + "'");
        channel.close();
        connection.close();
    }







}
